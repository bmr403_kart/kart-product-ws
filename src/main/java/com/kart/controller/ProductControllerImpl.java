package com.kart.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kart.entity.Product;
import com.kart.service.Impl.ProductServiceImpl;

import io.swagger.annotations.ApiOperation;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@RestController
@EnableSwagger2
@RequestMapping(value = "/product")
@ComponentScan(basePackages = { "com.kart.controller" })
public class ProductControllerImpl {

	@Autowired
	private ProductServiceImpl productServiceImpl;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@GetMapping(value = "/all")
	@ApiOperation(value = "Get All Products", nickname = "GET Products", 
	notes = "This endpoint retrieves Hello World", produces = "application/json")
	public List<Product> getAllProducts(@RequestHeader(value="Accept") String accept, @RequestHeader(value="Accept-Language") String acceptLanguage,
			@RequestHeader(value="User-Agent", defaultValue="foo") String userAgent, HttpServletRequest request ,  HttpServletResponse response ) throws Exception {
		logger.info("I'm in getHelloWorld() to return Hello World!");
		List<Product> products = productServiceImpl.getAllProducts();
		logger.info("I'm in getHelloWorld() "+products.size());
		return products;
	}
	
	@PostMapping(value = "/findByIds")
	@ApiOperation(value = "Get All Products ById", nickname = "GET Products", 
	notes = "This endpoint retrieves Get Products ById", produces = "application/json")
	public List<Product> getAllProductsById(@RequestBody List<Product> products, @RequestHeader(value="Accept") String accept, @RequestHeader(value="Accept-Language") String acceptLanguage,
			@RequestHeader(value="User-Agent", defaultValue="foo") String userAgent, HttpServletRequest request ,  HttpServletResponse response ) throws Exception {
		logger.info("I'm in getAllProductsById() to return Hello World!");
		List<Product> returnedProducts = productServiceImpl.findAllByIds(products.stream().map(Product::getProductId).collect(Collectors.toList()));
		logger.info("I'm in getAllProductsById() "+returnedProducts.size());
		return returnedProducts;
	}
	
	@PostMapping(value = "/find")
	@ApiOperation(value = "Get single Product", nickname = "GET single Product", 
	notes = "This endpoint retrieves single Product", produces = "application/json")
	public Product getProduct(@RequestBody Product product, @RequestHeader(value="Accept") String accept, @RequestHeader(value="Accept-Language") String acceptLanguage,
			@RequestHeader(value="User-Agent", defaultValue="foo") String userAgent, HttpServletRequest request ,  HttpServletResponse response ) throws Exception {
		logger.info("I'm in getProduct() to return Hello World!");
		Product resultedProduct = productServiceImpl.getOne(product.getProductId());
		logger.info("I'm in getProduct() "+resultedProduct.getProductName());
		return resultedProduct;
	}
	
	@PostMapping(value = "/findByProductName")
	@ApiOperation(value = "Get single Product", nickname = "GET single Product", 
	notes = "This endpoint retrieves single Product", produces = "application/json")
	public List<Product> getProductByNme(@RequestBody Product product, @RequestHeader(value="Accept") String accept, @RequestHeader(value="Accept-Language") String acceptLanguage,
			@RequestHeader(value="User-Agent", defaultValue="foo") String userAgent, HttpServletRequest request ,  HttpServletResponse response ) throws Exception {
		logger.info("I'm in getProductByNme() to return Hello World!");
		List<Product> resultedProducts = productServiceImpl.findByProductName(product.getProductName());
		logger.info("I'm in getProductByNme() "+resultedProducts.size());
		return resultedProducts;
	}
	
	@PostMapping(value = "/findBySeller")
	@ApiOperation(value = "Get list of Products by Seller", nickname = "GET list of Products by seller", 
	notes = "This endpoint retrieves products by Seller", produces = "application/json")
	public List<Product> findBySeller(@RequestBody Product product, @RequestHeader(value="Accept") String accept, @RequestHeader(value="Accept-Language") String acceptLanguage,
			@RequestHeader(value="User-Agent", defaultValue="foo") String userAgent, HttpServletRequest request ,  HttpServletResponse response ) throws Exception {
		logger.info("I'm in findBySeller() to return Hello World!");
		List<Product> resultedProducts = productServiceImpl.findBySeller(product.getSeller());
		logger.info("I'm in findBySeller() "+resultedProducts.size());
		return resultedProducts;
	}
	
	@PostMapping(value = "/findByOffers")
	@ApiOperation(value = "Get products by Offers", nickname = "GET products By Offers", 
	notes = "This endpoint retrieves single Product", produces = "application/json")
	public List<Product> findByOffers(@RequestBody Product product, @RequestHeader(value="Accept") String accept, @RequestHeader(value="Accept-Language") String acceptLanguage,
			@RequestHeader(value="User-Agent", defaultValue="foo") String userAgent, HttpServletRequest request ,  HttpServletResponse response ) throws Exception {
		logger.info("I'm in findByOffers() to return Hello World!");
		List<Product> resultedProducts = productServiceImpl.findByOffers(product.getOffers());
		logger.info("I'm in findByOffers() "+resultedProducts.size());
		return resultedProducts;
	}
	
	@PostMapping(value = "/findByCategory")
	@ApiOperation(value = "Get single Product", nickname = "GET single Product", 
	notes = "This endpoint retrieves single Product", produces = "application/json")
	public List<Product> findByCategory(@RequestBody Product product, @RequestHeader(value="Accept") String accept, @RequestHeader(value="Accept-Language") String acceptLanguage,
			@RequestHeader(value="User-Agent", defaultValue="foo") String userAgent, HttpServletRequest request ,  HttpServletResponse response ) throws Exception {
		logger.info("I'm in findByCategory() to return Hello World!");
		List<Product> resultedProducts = productServiceImpl.findByProductCategory(product.getProductCategory());
		logger.info("I'm in findByCategory() "+resultedProducts.size());
		return resultedProducts;
	}
	
	@PostMapping(value = "/save")
	@ApiOperation(value = "Save a product", nickname = "Saves a product", 
	notes = "This endpoint saves a product", produces = "application/json")
	public Product saveProduct(@RequestBody Product product, @RequestHeader(value="Accept") String accept, @RequestHeader(value="Accept-Language") String acceptLanguage,
			@RequestHeader(value="User-Agent", defaultValue="foo") String userAgent, HttpServletRequest request ,  HttpServletResponse response ) throws Exception {
		logger.info("I'm in saveProduct() to return Hello World!");
		Product savedProduct = productServiceImpl.saveAndFlush(product);
		logger.info("I'm in saveProduct() "+savedProduct);
		return savedProduct;
	}
	
	@PostMapping(value = "/saveAll")
	@ApiOperation(value = "Save All Products", nickname = "Saves list of products", 
	notes = "This endpoint saves All Products", produces = "application/json")
	public List<Product> saveAllProducts(@RequestBody List<Product> products, @RequestHeader(value="Accept") String accept, @RequestHeader(value="Accept-Language") String acceptLanguage,
			@RequestHeader(value="User-Agent", defaultValue="foo") String userAgent, HttpServletRequest request ,  HttpServletResponse response ) throws Exception {
		logger.info("I'm in saveAllProducts() to return Hello World!");
		List<Product> savedProducts = productServiceImpl.saveAll(products);
		logger.info("I'm in saveAllProducts() "+savedProducts.size());
		return savedProducts;
	}
	
	@DeleteMapping(value = "/delete")
	@ApiOperation(value = "To Delete Products", nickname = "Delete Products", 
	notes = "This endpoint retrieves Hello World", produces = "application/json")
	public List<Product> deleteProducts(@RequestBody List<Product> products, @RequestHeader(value="Accept") String accept, @RequestHeader(value="Accept-Language") String acceptLanguage,
			@RequestHeader(value="User-Agent", defaultValue="foo") String userAgent, HttpServletRequest request ,  HttpServletResponse response ) throws Exception {
		logger.info("I'm in deleteProducts() to return Hello World!");
		productServiceImpl.deleteInBatch(products);
		return products;
	}
	
	
	
}
