# app-mu-product-ws ![CI status](https://img.shields.io/badge/build-passing-brightgreen.svg)

app-mu-product-ws is an spring boot framework java application where API endpoints have been created for manufacturing unit application product layer.


## Build

* mvn clean install

## Run

* mvn spring-boot:run

### Requirements
* Java 8
* Maven ~> 3
* Git ~> 2

## Usage

 <dependency> </dependency>

## Development


## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.


